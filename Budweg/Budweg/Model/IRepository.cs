﻿using System.Collections.Generic;

namespace Budweg.Model
{
    public interface IRepository<T> where T : class
    {
        protected static string connection;

        // connection string available to all that impliment interface..... "IRepository<T>.connection"... and yeah it works :p

        static IRepository() => connection = "Server=10.56.8.35;Database=A_GRUPDB03_2020;User Id=A_GROUP03;Password=A_OPENDB03";

        IEnumerable<T> GetAll();

        T GetById(object id);

        void Add(T obj);

        void Update(T obj, T newValues);

        void Delete(T obj);
    }
}
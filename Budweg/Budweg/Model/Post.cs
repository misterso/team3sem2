﻿using System;

namespace Budweg.Model
{
    public class Post
    {
        public int PostID { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public string Author { get; set; }

        public DateTime Date { get; set; }

        public string Category { get; set; }

        public string Status { get; set; }

        public bool IsAnonymous { get; set; }

        public Post(bool IsAnonymous = false) => this.IsAnonymous = IsAnonymous;
    }
}
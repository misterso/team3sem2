﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Budweg.Model
{
    public class PostRepo : IRepository<Post>
    {
        private List<Post> posts;

        public PostRepo()
        {
            posts = new List<Post>();
            LoadRepo();
        }

        public IEnumerable<Post> GetAll() => posts;

        public Post GetById(object id) => posts.First(item => item.PostID == (int)id);

        public void Add(Post post)
        {
            //Adder til C# listen
            posts.Add(post);

            //Adder til databasen
            using (SqlConnection sc = new SqlConnection(IRepository<Post>.connection))
            {
                sc.Open();

                string query = "INSERT INTO Post ([Username], [Title], [Message], [Date], [CategoryName], [StatusName], [IsAnonymous]) " +
                    "VALUES (@Username, @Title, @Message, @Date, @CategoryName, @StatusName, @IsAnonymous)";

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    command.Parameters.Add("Username", System.Data.SqlDbType.VarChar).Value = post.Author;
                    command.Parameters.Add("Title", System.Data.SqlDbType.VarChar).Value = post.Title;
                    command.Parameters.Add("Message", System.Data.SqlDbType.VarChar).Value = post.Message;
                    command.Parameters.Add("Date", System.Data.SqlDbType.DateTime2).Value = post.Date;
                    command.Parameters.Add("CategoryName", System.Data.SqlDbType.VarChar).Value = post.Category;
                    command.Parameters.Add("StatusName", System.Data.SqlDbType.VarChar).Value = post.Status;
                    command.Parameters.Add("IsAnonymous", System.Data.SqlDbType.Bit).Value = post.IsAnonymous;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Update(Post post, Post updated)
        {
            //Opdater C# listen
            foreach (Post item in posts)
            {
                if (item.PostID == post.PostID)
                {
                    item.Title = updated.Title;
                    item.Message = updated.Message;
                    item.Category = updated.Category;
                    item.Status = updated.Status;
                }
            }

            //Opdaterer Databasen
            using (SqlConnection sc = new SqlConnection(IRepository<Post>.connection))
            {
                sc.Open();

                string query = "UPDATE Post SET [Title] = @Title, [Message] = @Message, [CategoryName] = @CategoryName, [StatusName] = @StatusName, [IsAnonymous] = @IsAnonymous " +
                    "WHERE PostID = " + post.PostID;

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    command.Parameters.Add("Title", System.Data.SqlDbType.VarChar).Value = updated.Title;
                    command.Parameters.Add("Message", System.Data.SqlDbType.VarChar).Value = updated.Message;
                    command.Parameters.Add("CategoryName", System.Data.SqlDbType.VarChar).Value = updated.Category;
                    command.Parameters.Add("StatusName", System.Data.SqlDbType.VarChar).Value = updated.Status;
                    command.Parameters.Add("IsAnonymous", System.Data.SqlDbType.Bit).Value = updated.IsAnonymous;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Delete(Post post)
        {
            //Sletter fra C# listen
            posts.Remove(post);

            //Sletter fra C# databasen
            using (SqlConnection sc = new SqlConnection(IRepository<Post>.connection))
            {
                sc.Open();

                string query = "DELETE FROM " + "Post" + " WHERE " + "PostID" + " = " + post.PostID;

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public void LoadRepo()
        {
            //Nulstiller posts
            posts.Clear();

            //Indsætter data fra database
            using (SqlConnection sc = new SqlConnection(IRepository<Post>.connection))
            {
                sc.Open();

                string query = "SELECT PostID, Title, Message, Username, Date, CategoryName, StatusName, IsAnonymous FROM Post";

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Post post = new Post((bool)reader["IsAnonymous"]);

                            post.PostID = (int)reader["PostID"];
                            post.Title = (string)reader["Title"];
                            post.Message = (string)reader["Message"];
                            post.Author = (string)reader["Username"];
                            post.Date = (DateTime)reader["Date"];
                            post.Category = (string)reader["CategoryName"];
                            post.Status = (string)reader["StatusName"];
                            posts.Add(post);
                        }
                    }
                }
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Budweg.Model

{
    // Enum replacement class for 1 columm tables in database
    public class StringRepo : IRepository<string>
    {
        private List<string> items;
        private string table;
        private string column;

        // initialises list of strings and the table name, uses FindColumnName to find column name and loads the data into items list.

        public StringRepo(string _table)
        {
            items = new List<string>();
            table = _table;
            FindColumnName();
            LoadRepo();
        }

        // CRUD for both database and repo

        public void Add(string obj)
        {
            items.Add(obj);

            using (SqlConnection sc = new SqlConnection(IRepository<string>.connection))
            {
                sc.Open();

                string query = $"INSERT INTO {table} ({column}) VALUES ('{obj}')";

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    command.ExecuteNonQuery();
                }
                sc.Close();
            }
        }

        public void Delete(string obj)
        {
            items.Remove(obj);

            using (SqlConnection sc = new SqlConnection(IRepository<string>.connection))
            {
                sc.Open();

                string query = $"DELETE FROM {table} WHERE {column} = '{obj}' ";

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    command.ExecuteNonQuery();
                }
                sc.Close();
            }
        }

        public IEnumerable<string> GetAll() => items;

        public string GetById(object id) => items.SingleOrDefault(i => i == (string)id);

        public void Update(string obj, string newValues)
        {
            items[items.FindIndex(i => i == obj)] = newValues;

            using (SqlConnection sc = new SqlConnection(IRepository<string>.connection))
            {
                sc.Open();

                string query = $"UPDATE {table} SET {column} = '{newValues}' WHERE {column} = '{obj}'";

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    command.ExecuteNonQuery();
                }
                sc.Close();
            }
        }

        // Reads Column into items list.

        public void LoadRepo()
        {
            using (SqlConnection sc = new SqlConnection(IRepository<string>.connection))
            {
                sc.Open();

                string query = $"SELECT {column} FROM {table}";

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            items.Add(reader[$"{column}"].ToString());
                        }
                    }
                }
                sc.Close();
            }
        }

        // Finds the column name for the table from the INFORMATION SCHEMA of the Database

        public void FindColumnName()
        {
            using (SqlConnection sc = new SqlConnection(IRepository<string>.connection))
            {
                sc.Open();

                string query = $"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS " +
                    $"WHERE TABLE_NAME ='{table}' " +
                    $"AND ORDINAL_POSITION = 1";

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    column = command.ExecuteScalar().ToString();
                }
                sc.Close();
            }
        }
    }
}
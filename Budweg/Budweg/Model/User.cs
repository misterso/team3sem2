﻿namespace Budweg.Model
{
    public class User
    {
        private bool isAdmin;

        public string Username { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public string Department { get; set; }

        public User(bool isAdmin = false) => this.isAdmin = isAdmin;

        public bool GetAdminStatus() => isAdmin;
    }
}
﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Budweg.Model
{
    public class UserRepo : IRepository<User>
    {
        private List<User> users;

        public UserRepo()
        {
            users = new List<User>();
            LoadRepo();
        }

        public IEnumerable<User> GetAll() => users;

        public User GetById(object id) => users.First(item => item.Username == (string)id);

        public void Add(User user)
        {
            //Adder til C# listen
            users.Add(user);

            //Adder til databasen
            using (SqlConnection sc = new SqlConnection(IRepository<User>.connection))
            {
                sc.Open();

                string query = "INSERT INTO UserTable ([Username], [DepartmentName], [Name], [IsAdmin], [Password]) " +
                    "VALUES (@Username, @DepartmentName, @Name, @IsAdmin, @Password)";

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    command.Parameters.Add("Username", System.Data.SqlDbType.VarChar).Value = user.Username;
                    command.Parameters.Add("DepartmentName", System.Data.SqlDbType.VarChar).Value = user.Department;
                    command.Parameters.Add("Name", System.Data.SqlDbType.VarChar).Value = user.Name;
                    command.Parameters.Add("IsAdmin", System.Data.SqlDbType.DateTime2).Value = user.GetAdminStatus();
                    command.Parameters.Add("Password", System.Data.SqlDbType.Int).Value = user.Password;

                    command.ExecuteNonQuery();
                }
            }
        }

        public void Update(User user, User updated)
        {
            //Opdaterer C# listen
            foreach (User item in users)
            {
                if (item == user)
                {
                    item.Name = updated.Name;
                    item.Department = updated.Department;
                }
            }

            //Opdaterer Databasen
            using (SqlConnection sc = new SqlConnection(IRepository<User>.connection))
            {
                sc.Open();

                string query = "UPDATE UserTable(Name, DepartmentName) " +
                    "VALUES (@Name, @DepartmentName)" +
                    "WHERE Username = " + user.Username;

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    command.Parameters.Add("Name", System.Data.SqlDbType.VarChar).Value = updated.Name;

                    command.Parameters.Add("DepartmentName", System.Data.SqlDbType.Int).Value = updated.Department;

                    command.ExecuteNonQuery();
                }
            }
        }

        public void Delete(User user)
        {
            //Sletter fra C# listen
            users.Remove(user);

            //Sletter fra C# databasen
            using (SqlConnection sc = new SqlConnection(IRepository<User>.connection))
            {
                sc.Open();

                string query = "DELETE FROM UserTable WHERE Username = " + user.Username;

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public void LoadRepo()
        {
            using (SqlConnection sc = new SqlConnection(IRepository<Post>.connection))
            {
                sc.Open();

                string query = "SELECT Username, Name, IsAdmin, Password, DepartmentName FROM UserTable";

                using (SqlCommand command = new SqlCommand(query, sc))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User user = new User((bool)reader["IsAdmin"]);

                            user.Username = (string)reader["Username"];
                            user.Password = (string)reader["Password"];
                            user.Name = (string)reader["Name"];
                            user.Department = (string)reader["DepartmentName"];

                            users.Add(user);
                        }
                    }
                }
            }
        }
    }
}
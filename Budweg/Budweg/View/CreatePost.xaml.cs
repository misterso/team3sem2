﻿using System.Windows;
using System.Windows.Controls;

namespace Budweg.View
{
    /// <summary>
    /// Interaction logic for CreatePost.xaml
    /// </summary>
    public partial class CreatePost : Window
    {
        public CreatePost()
        {
            InitializeComponent();
            DataContext = MainMenu.mvm;
        }

        private void bttn_postComplete_Click(object sender, RoutedEventArgs e)
        {
            MainMenu.mvm.AddPost(tb_titleInput_post.Text, tb_postBodyInput.Text, category_selection.Text, MainMenu.mvm.IsAnonymous);
            this.Close();
        }

        //Removes the prompt message when focused.
        private void tb_postBodyInput_GotFocus(object sender, RoutedEventArgs e)
        {
            if (tb_postBodyInput.Text == "Indtast dit opslag")
                tb_postBodyInput.Text = "";
        }

        //Removes the prompt message when focused.
        private void tb_postBodyInput_LostFocus(object sender, RoutedEventArgs e)
        {
            if (tb_postBodyInput.Text == "")
                tb_postBodyInput.Text = "Indtast dit opslag";
        }

        private void tb_titleInput_post_GotFocus(object sender, RoutedEventArgs e)
        {
            if (tb_titleInput_post.Text == "Indtast titel")
                tb_titleInput_post.Text = "";
        }

        //Removes the prompt message when focused.
        private void tb_titleInput_post_LostFocus(object sender, RoutedEventArgs e)
        {
            if (tb_titleInput_post.Text == "")
                tb_titleInput_post.Text = "Indtast titel";
        }

        private void category_selection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void category_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.ItemsSource = MainMenu.mvm.GetStringList("category");
        }
    }
}
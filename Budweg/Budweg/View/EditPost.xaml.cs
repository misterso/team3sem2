﻿using System.Windows;
using System.Windows.Controls;

namespace Budweg.View
{
    /// <summary>
    /// Interaction logic for EditPost.xaml
    /// </summary>
    public partial class EditPost : Window
    {
        public EditPost()
        {
            InitializeComponent();
            DataContext = MainMenu.mvm;
        }

        private void bttn_postComplete_Click(object sender, RoutedEventArgs e)
        {
            MainMenu.mvm.UpdatePost(tb_titleInput_post.Text, tb_postBodyInput.Text, category_selection.Text, MainMenu.mvm.IsAnonymous);
            MainMenu.mvm.LoadVMs();

            this.Close();
        }

        private void category_selection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void category_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.ItemsSource = MainMenu.mvm.GetStringList("category");
        }
    }
}
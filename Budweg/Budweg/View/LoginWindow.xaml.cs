﻿using System;
using System.Windows;

namespace Budweg.View
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            DataContext = MainMenu.mvm;
            InitializeComponent();
        }

        private void bttn_login_Click(object sender, RoutedEventArgs e)
        {
            string username = tb_username_input.Text;
            string password = tb_passwordInput2.Password;
            try
            {
                MainMenu.mvm.LogIn(username, password);
                if (MainMenu.mvm.SelectedUser != null)
                {
                    MainMenu mainMenu = new MainMenu();
                    mainMenu.Show();
                    this.Close();
                }
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Forkerte informationer indtastet. Prøv igen.");
            }
        }
    }
}
﻿using Budweg.ViewModel;
using System.Windows;
using System.Windows.Controls;

namespace Budweg.View
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        public static MainViewModel mvm { get; } = new MainViewModel();

        public MainMenu()
        {
            InitializeComponent();
            DataContext = mvm;

            if (MainMenu.mvm.SelectedUser.GetAdminStatus() == false)
            {
                bttn_menuAllPosts.IsEnabled = false;
            }
        }

        private void listBox_menuPosts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void bttn_menuCreatePost_Click(object sender, RoutedEventArgs e)
        {
            CreatePost CreatePostDlg = new CreatePost();
            CreatePostDlg.ShowDialog();
        }

        private void bttn_menuMyPosts_Click(object sender, RoutedEventArgs e)
        {
            listBox_menuPosts.ItemsSource = mvm.UserPosts;
        }

        private void bttn_menuAllPosts_Click(object sender, RoutedEventArgs e)
        {
            listBox_menuPosts.ItemsSource = mvm.AllPosts;
        }

        private void bttn_menuLogOff_Click(object sender, RoutedEventArgs e)
        {
            Window1 LoginWindowDlg = new Window1();
            LoginWindowDlg.Show();
            this.Close();
        }

        private void bttn_deletePosts_Click(object sender, RoutedEventArgs e)

        {
            mvm.SelectedPost = (PostVM)((ListBoxItem)listBox_menuPosts.ContainerFromElement((Button)sender)).Content;
            MainMenu.mvm.DeletePost(mvm.SelectedPost);
        }

        private void bttn_editPosts_Click(object sender, RoutedEventArgs e)

        {
            mvm.SelectedPost = (PostVM)((ListBoxItem)listBox_menuPosts.ContainerFromElement((Button)sender)).Content;
            EditPost EditPostDlg = new EditPost();
            EditPostDlg.ShowDialog();
        }

        private void chkBox_category_sorting_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            for (int i = 0; i < mvm.AllPosts.Count; i++)
            {
                if (mvm.AllPosts[i].Category == chkBox_category_sorting.SelectedItem.ToString())
                {
                    mvm.AllPosts.Move(i, 0);
                }
            }

            for (int i = 0; i < mvm.UserPosts.Count; i++)
            {
                if (mvm.UserPosts[i].Category == chkBox_category_sorting.SelectedItem.ToString())
                {
                    mvm.UserPosts.Move(i, 0);
                }
            }
        }

        private void chkBox_category_sorting_Loaded(object sender, RoutedEventArgs e)
        {
            var combo = sender as ComboBox;
            combo.ItemsSource = mvm.GetStringList("category");
        }
    }
}
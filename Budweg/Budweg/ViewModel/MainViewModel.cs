﻿using Budweg.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Budweg.ViewModel
{
    public class MainViewModel
    {
        private PostRepo postRepo;
        private UserRepo userRepo;
        private StringRepo departmentRepo;
        private StringRepo categoryRepo;
        private StringRepo statusRepo;

        public bool IsAnonymous { get; set; }
        public IEnumerable<string> Category { get; set; }

        public MainViewModel()
        {
            postRepo = new PostRepo();
            userRepo = new UserRepo();
            departmentRepo = new StringRepo("Department");
            categoryRepo = new StringRepo("Category");
            statusRepo = new StringRepo("Status");
            IsAnonymous = false;
            Category = categoryRepo.GetAll();
            allPosts = new ObservableCollection<PostVM>();
            userPosts = new ObservableCollection<PostVM>();
        }

        public List<string> GetStringList(string list)

        {
            return list switch
            {
                "department" => departmentRepo.GetAll() as List<string>,
                "category" => categoryRepo.GetAll() as List<string>,
                "status" => statusRepo.GetAll() as List<string>,
                _ => null
            };
        }

        private PostVM selectedPost;

        public PostVM SelectedPost
        {
            get { return selectedPost; }
            set { selectedPost = value; }
        }

        private User selectedUser;

        public User SelectedUser
        {
            get { return selectedUser; }
            set { selectedUser = value; }
        }

        private ObservableCollection<PostVM> userPosts;

        public ObservableCollection<PostVM> UserPosts
        {
            get { return userPosts; }
            set { userPosts = value; }
        }

        private ObservableCollection<PostVM> allPosts;

        public ObservableCollection<PostVM> AllPosts
        {
            get { return allPosts; }
            set { allPosts = value; }
        }

        public void AddPost(string title, string message, string category, bool isAnonymous)
        {
            //Opretter et Post objekt og kalder postRepo.Add metoden med post som parameteren
            Post post = new Post()
            {
                Title = title,
                Message = message,
                Author = SelectedUser.Username,
                Category = category,
                Date = DateTime.Now,
                Status = "Ulæst",
                IsAnonymous = isAnonymous
            };

            postRepo.Add(post);

            LoadVMs();
        }

        public void UpdatePost(string titel, string message, string category, bool isAnonymous)
        {
            Post post = new Post();
            post.Title = titel;
            post.Message = message;
            post.Category = category;
            post.Status = selectedPost.Status;
            post.IsAnonymous = isAnonymous;

            //Sætter ulæst til læst
            if (SelectedUser.GetAdminStatus())
            {
                if (SelectedUser.Username != SelectedPost.Author)
                {
                    post.Status = "Læst";
                }
            }
            postRepo.Update(postRepo.GetById(SelectedPost.PostID), post);
            LoadVMs();
        }

        public void DeletePost(PostVM postVM)
        {
            postRepo.Delete(postRepo.GetById(postVM.PostID));
            LoadVMs();
        }

        public void AddUser()
        {
            throw new NotImplementedException();
        }

        public void UpdateUser()
        {
            throw new NotImplementedException();
        }

        public void DeleteUser()
        {
            throw new NotImplementedException();
        }

        public void LogIn(string username, string password)
        {
            selectedUser = null;

            foreach (User item in userRepo.GetAll())
            {
                if (item.Username == username && item.Password == password) selectedUser = item;
                if (selectedUser != null)
                {
                    LoadVMs();
                    break;
                }
            }
            if (selectedUser == null) throw new ArgumentException();
        }

        public void LoadVMs()
        {
            //Reloader repo
            postRepo.LoadRepo();

            //Nulstiller listen
            allPosts.Clear();
            userPosts.Clear();

            //Adder alle post fra postRepo/Database til PostVM-listen
            foreach (Post item in postRepo.GetAll())
            {
                PostVM postVM = new PostVM();

                postVM.PostID = item.PostID;
                postVM.Title = item.Title;
                postVM.Message = item.Message;
                postVM.Author = item.Author;
                postVM.Date = item.Date;
                postVM.Category = item.Category;
                postVM.Status = item.Status;
                postVM.IsAnonymous = item.IsAnonymous;

                //Hvis "post" tilhører "SelectedUser" bliver post tilføjet til "UserPosts" også
                if (item.Author == SelectedUser.Username) UserPosts.Add(postVM);
                if (postVM.IsAnonymous == true) postVM.Author = "Anonym";
                AllPosts.Add(postVM);
            }
        }
    }
}
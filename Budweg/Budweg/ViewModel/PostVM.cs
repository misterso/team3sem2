﻿using System;
using System.ComponentModel;

namespace Budweg.ViewModel
{
    public class PostVM : INotifyPropertyChanged
    {
        private int postID;

        public int PostID
        {
            get { return postID; }
            set
            {
                postID = value;
                OnPropertyChanged("Title");
            }
        }

        private string title;

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged("Title");
            }
        }

        private string message;

        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                OnPropertyChanged("Message");
            }
        }

        private string author;

        public string Author
        {
            get { return author; }
            set
            {
                author = value;
                OnPropertyChanged("Message");
            }
        }

        private DateTime date;

        public DateTime Date
        {
            get { return date; }
            set
            {
                date = value;
                OnPropertyChanged("Date");
            }
        }

        private string category;

        public string Category
        {
            get { return category; }
            set
            {
                category = value;
                OnPropertyChanged("Category");
            }
        }

        private string status;

        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                OnPropertyChanged("Status");
            }
        }

        private bool isAnonymous;

        public bool IsAnonymous
        {
            get { return isAnonymous; }
            set
            {
                isAnonymous = value;
                OnPropertyChanged("IsAnonymous");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}